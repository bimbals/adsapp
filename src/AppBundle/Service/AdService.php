<?php
namespace AppBundle\Service;

use AppBundle\Entity\AdEntity;
use AppBundle\Entity\UserEntity;
use AppBundle\Repository\AdRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AdService
{
    private $repository;
    private $tokenStorage;
    private $em;

    public function __construct(AdRepository $repository, TokenStorage $tokenStorage, EntityManager $em)
    {
        $this->repository = $repository;
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }

    public function getAll()
    {
        return $this->repository->findAll();
    }

    public function getByUser(UserEntity $user)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();

        // throw exception if user tries to see other user lists
        if($currentUser && $user->getId() != $currentUser->getId()) {
            throw new ResourceNotFoundException('');
        }

        return $this->repository->findByUser($user);
    }

    public function initialize() {
        return new AdEntity();
    }

    public function save(AdEntity $ad) {

        $ad->setUser($this->tokenStorage->getToken()->getUser());

        $this->em->persist($ad);
        $this->em->flush();
    }
}
