<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserEntity;
use AppBundle\Form\AdType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\AdService;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdController extends Controller
{
    /**
     * @Route("/", name="list_ad", defaults={"user": null})
     * @Route("/list/{user}", name="list_ad_by_user")
     *
     * @param UserEntity $user
     *
     * @return Response
     */
    public function indexAction(UserEntity $user = null)
    {
        $adService = $this->get('app.business.ad');

        if(null !== $user) {
            $ads = $adService->getByUser($user);
        } else {
            $ads = $adService->getAll();
        }

        // replace this example code with whatever you need
        return $this->render('default/list_ad.html.twig', array(
            'ads' => $ads,
        ));
    }

    /**
     * @Security("is_authenticated()")
     *
     * @Route("/ad/new", name="new_ad")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request)
    {
        $adService = $this->get('app.business.ad');

        // create new AdEntity
        $ad = $adService->initialize();

        $form = $this->createForm(new AdType(), $ad);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $adService->save($ad);

            $this->addFlash('success', 'notice.success');

            return $this->redirectToRoute('list_ad');
        }

        return $this->render(
            'default/new_ad.html.twig',
            ['form' => $form->createView()]
        );
    }

}
